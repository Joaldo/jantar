package filosofos.jantar;

import java.util.ArrayList;
import java.util.Random;

public class Filosofos {
	
	private int garfo1;
	private int garfo2;
	private int garfo3;
	private int garfo4;
	private int garfo5;
	
	
	//m�todos getters e setters somente para uso das variveis  
	public int getGarfo1() {
		return garfo1;
	}

	public void setGarfo1(int garfo1) {
		
		this.garfo1 = garfo1;
	}

	public int getGarfo2() {
		return garfo2;
	}

	public void setGarfo2(int garfo2) {
		this.garfo2 = garfo2;
	}

	public int getGarfo3() {
		return garfo3;
	}

	public void setGarfo3(int garfo3) {
		this.garfo3 = garfo3;
	}

	public int getGarfo4() {
		return garfo4;
	}

	public void setGarfo4(int garfo4) {
		this.garfo4 = garfo4;
	}

	public int getGarfo5() {
		return garfo5;
	}

	public void setGarfo5(int garfo5) {
		this.garfo5 = garfo5;
	}
	//Exportando o m�todo throws dentro do m�todo void para usar o Thread
	public void nomearFilosofos() throws InterruptedException{
		
		
		
		Random rd = new Random();
		
		//Criando array com os nomes dos filosofos
		ArrayList<String> filosofos = new ArrayList<>();
		filosofos.add("Plat�o");
		filosofos.add("Aristoteles");
		filosofos.add("Sun Tzu");
		filosofos.add("Descartes");
		filosofos.add("Kant");
		
		
		//Atribuindo valor aleatrio de 1 a 5 a uma variavel
		int c = rd.nextInt(5);
		
		//Estutura para n�o mostrar nome do filosofo que ir comer
		for (int i = 0; i < 5; i++) {
			if(filosofos.get(i) == filosofos.get(c)){
				
			}else{
				System.out.println(filosofos.get(i)+ " est� filosofando.");		
				
			}
		}
		
		//Estrutura usada para mostrar quais garfos o filosofo que vai comer pegará
		for (int i = 0; i < 1; i++) {
						
			System.out.println(filosofos.get(c) + " EST� COM FOME");
			Thread.sleep(2000);//Contador 
			
			if(filosofos.get(c) == "Plat�o") {
				System.out.println(filosofos.get(0) + " Pegou o garfo " + getGarfo1() + " e o " + getGarfo2());
			}else if(filosofos.get(c) == "Aristoteles") {
				System.out.println(filosofos.get(1) + " Pegou o garfo " + getGarfo2() + " e o " + getGarfo3());
			}else if(filosofos.get(c) == "Sun Tzu") {
				System.out.println(filosofos.get(2) + " Pegou o garfo " + getGarfo3() + " e o " + getGarfo4());
			}else if(filosofos.get(c) == "Descartes") {
				System.out.println(filosofos.get(3) + " Pegou o garfo " + getGarfo4() + " e o " + getGarfo5());
			}else if(filosofos.get(c) == "Kant") {
				System.out.println(filosofos.get(4) + " Pegou o garfo " + getGarfo5() + " e o " + getGarfo1());
			}
			Thread.sleep(500);
		}
		Thread.sleep(1000);
		//Mostrando que o filosofo terminou de comer
		System.out.println(filosofos.get(c) + " Terminou de comer." + "\n");
	}
}
